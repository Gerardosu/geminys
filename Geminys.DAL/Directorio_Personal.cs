//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Geminys.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class Directorio_Personal
    {
        public int idDirectorioPersonal { get; set; }
        public string nombre { get; set; }
        public int idEmpresa { get; set; }
        public string servicio { get; set; }
        public string puesto { get; set; }
        public string email { get; set; }
        public string fijo { get; set; }
        public string movilCorporativo { get; set; }
        public string movilLargo { get; set; }
        public string comentarios { get; set; }
        public Nullable<bool> guardias { get; set; }
        public Nullable<bool> activo { get; set; }
    
        public virtual Empresas Empresas { get; set; }
    }
}
