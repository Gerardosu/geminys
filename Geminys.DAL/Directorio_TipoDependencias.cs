//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Geminys.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class Directorio_TipoDependencias
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Directorio_TipoDependencias()
        {
            this.Directorio_Instalaciones_Externas = new HashSet<Directorio_Instalaciones_Externas>();
            this.Directorio_Instalaciones_Internas = new HashSet<Directorio_Instalaciones_Internas>();
        }
    
        public int idTipoDependencia { get; set; }
        public string TipoDependencia { get; set; }
        public Nullable<bool> activo { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Directorio_Instalaciones_Externas> Directorio_Instalaciones_Externas { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Directorio_Instalaciones_Internas> Directorio_Instalaciones_Internas { get; set; }
    }
}
