//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Geminys.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class Mettas
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Mettas()
        {
            this.Incidencias_Mettas = new HashSet<Incidencias_Mettas>();
        }
    
        public int idMetta { get; set; }
        public System.DateTime fechaSistema { get; set; }
        public string dne { get; set; }
        public int numero { get; set; }
        public int idDependencia { get; set; }
        public Nullable<int> precinto { get; set; }
        public Nullable<System.DateTime> fechaModificacion { get; set; }
        public bool activo { get; set; }
        public bool vigilante { get; set; }
    
        public virtual Dependencias Dependencias { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Incidencias_Mettas> Incidencias_Mettas { get; set; }
    }
}
