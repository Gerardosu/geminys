﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;

using System.Threading.Tasks;

namespace Geminys.DAL.Users
{
   public  class UsersProvider
    {
        /// <summary>
        /// Metodo que devuelve un listado con todos los usuarios dados de alta en Geminys
        /// <returns>Objeto de tipo GEMINYS.DAL.USUARIOS</returns>
        /// </summary>
        /// <returns></returns>
        public List<Usuarios> GetListadoUsuarios()
        {
            using (var context = new GeminysEntities())
            {
                // var listadoUsuarios = context.Usuarios.Include("Perfil").Include("Empresas").Where(x => x.activo == true).ToList();
                var listadoUsuarios = context.Usuarios.Include("Perfil").Include("Empresas").ToList();
                return listadoUsuarios;
            }
        }

        /// <summary>
        /// Metodo que devuelve un listado con los usuarios que coinciden con el criterio de busqueda
        /// </summary>
        /// <param name="nombre"></param>
        /// <returns></returns>
        public List<Usuarios> FindUsuarios(string nombre,string apellido1, string apellido2,string dne,bool? activo)
        {
            using (var context = new GeminysEntities())
            {
                var listadoUsuarios = context.Usuarios.Include("Perfil").Include("Empresas").Where(x => (x.nombre.Equals(nombre) || x.apellido1.Equals(apellido1) || x.apellido2.Equals(apellido2) || x.dne.Equals(dne) || x.activo == activo)).ToList();
                return listadoUsuarios;
            }
        }


        /// <summary>
        /// Metodo para obtener los usuarios conectados en una fecha determinada
        /// </summary>
        /// <param name="fecha"></param>
        /// <returns></returns>
        public List<SesionUsuarios> FindUsuariosConectados(string fecha)
        {

            DateTime dtFechaMinima = DateTime.Parse(fecha);
            DateTime dtFechaMaxima = dtFechaMinima.AddDays(1);

            List<SesionUsuarios> usuariosConectados = null;

            try
            {
                using (var context = new GeminysEntities())
                {
                    usuariosConectados = context.SesionUsuarios.Include("Usuarios").Where(x => x.horaInicio >= dtFechaMinima && x.horaInicio <= dtFechaMaxima).ToList();

                }


                return usuariosConectados;
            }

            catch (Exception ex)
            {
                Debug.WriteLine("{0}{1}", ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Metodo que realiza el alta de un usuario
        /// </summary>
        /// <param name="nombreAlta"></param>
        /// <param name="apellido1Alta"></param>
        /// <param name="apellido2Alta"></param>
        /// <param name="dneAlta"></param>
        /// <param name="tipAlta"></param>
        /// <param name="empresaAlta"></param>
        /// <param name="perfilAlta"></param>
        /// <param name="estadoAlta"></param>
        public static bool AltaUsuario(string nombre, string apellido1, string apellido2, string dne, string tip, string empresa, string perfil, string telefono)
        {
            bool altaCorrecta = false;
            try
            {
                using (var context = new GeminysEntities())
                {

                    bool existe = context.Usuarios.Any(u => u.dne ==  dne);

                    altaCorrecta = false;

                    if (!existe)
                    {
                        var usuario = context.Set<Usuarios>();
                        usuario.Add(new Usuarios { nombre = nombre, apellido1 = apellido1, apellido2 = apellido2, dne = dne, tip = tip, idPerfil = Convert.ToInt32(perfil), idEmpresa = Convert.ToInt32(empresa), activo = true, telefono = telefono });
                        context.SaveChanges();
                        altaCorrecta = true;
                    }

                }
                return altaCorrecta;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("{0}{1}", ex.Message);
                return false;
            }
       }
        /// <summary>
        /// Metodo para activar o desactivar un usuario.
        /// </summary>
        /// <param name="idUsuario"></param>
        /// <param name="operacion">Si el parametro es A se activara el usuario, si es una D se desactivara</param>
        /// <returns></returns>
        public static bool ActivarDesactivarUsuario(string idUsuario,string operacion)
        {
            bool modificacionCorrecta = false;
            int usuario = Convert.ToInt32(idUsuario);
            try
            {
                using (var context = new GeminysEntities())
                {
                    var registro = context.Usuarios.SingleOrDefault(x => x.idUsuario == usuario);
                    if (registro != null)
                    {
                        if (operacion == "A")
                        {
                            registro.activo = true;
                        }
                        else {

                            registro.activo = false;
                        }
                       context.SaveChanges();
                       modificacionCorrecta = true;
                    }
                }
                return modificacionCorrecta;
            }

            catch (Exception ex)
            {
                Debug.WriteLine("{0}{1}", ex.Message);
                return false;
            }
        }



        /// <summary>
        /// Metodo para Modificar los datos de un usario
        /// </summary>
        /// <param name="nombreAlta"></param>
        /// <param name="apellido1Alta"></param>
        /// <param name="apellido2Alta"></param>
        /// <param name="dneAlta"></param>
        /// <param name="tipAlta"></param>
        /// <param name="empresaAlta"></param>
        /// <param name="perfilAlta"></param>
        /// <param name="telefonoAlta"></param>
        /// <returns></returns>
        public static bool ModificarUsuario(string nombre, string apellido1, string apellido2, string dne, string tip, string empresa, string perfil, string telefono)
        {
            bool modificacionCorrecta = false;
            try
            {
                using (var context = new GeminysEntities())
                {
                    var registro = context.Usuarios.SingleOrDefault(x => x.dne == dne);
                    if (registro != null)
                    {
                        registro.nombre = nombre;
                        registro.apellido1 = apellido1;
                        registro.apellido2 = apellido2;
                        registro.dne = dne;
                        registro.telefono = telefono;
                        registro.tip = tip;
                        registro.idEmpresa = Convert.ToInt32(empresa);
                        registro.idPerfil = Convert.ToInt32(perfil);
                        context.SaveChanges();
                        modificacionCorrecta = true;
                    }
                }
                return modificacionCorrecta;
            }
            
            catch (Exception ex)
            {
                Debug.WriteLine("{0}{1}", ex.Message);
                return false;
            }
         }
    }

}
