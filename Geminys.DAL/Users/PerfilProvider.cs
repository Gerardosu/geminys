﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity.Core.Objects;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Data.Entity;


namespace Geminys.DAL.Users
{
    public class PerfilProvider
    {
        public int AddPerfil(string nombre)
        {
            try
            {
                using (var context = new GeminysEntities())
                {
                    var perfil = context.Perfil.SingleOrDefault(p => p.nombrePerfil == nombre);
                    var listaMenu = (from m in context.Menu
                                 join s in context.Menu on m.idMenu equals s.idMenu into AllMenu
                                 from menu in AllMenu.DefaultIfEmpty()
                                 orderby menu.idPadre, menu.orden
                                 select menu).ToList();

                    if (perfil == null)
                    {                     
                        //Si no existe un perfil con el mismo nombre, entonces se crea uno nuevo
                        perfil = new Perfil
                        {
                            nombrePerfil = nombre,
                            activo = true,
                        };
                        context.Perfil.Add(perfil);
                        context.SaveChanges();
                        //obtenemos el nuevo idPerfil 
                        //int idPerfilNuevo = perfil.idPerfil;

                        //var perfil_Menu = context.Perfil_Menu.SingleOrDefault(pm => pm.idPerfil == idPerfilNuevo);
                        //if (perfil_Menu == null)
                        //{
                        //    //asignamos todo el menu y submenu  al nuevo Perfil
                        //    //foreach (var menuItem in listaMenu)
                        //    foreach (Menu menuItem in listaMenu)
                        //    {
                        //        perfil_Menu = new Perfil_Menu();

                        //        perfil_Menu.idMenu = menuItem.idMenu;
                        //        perfil_Menu.idPerfil = idPerfilNuevo;
                        //        context.Perfil_Menu.Add(perfil_Menu);
                        //        context.SaveChanges();
                        //    }                           
                        //}  
                        return perfil.idPerfil;
                    }
                    else
                    {
                        return 0;
                    }
                    
                }
            }
            catch(Exception ex)
            {
                Debug.WriteLine("{0}", ex.Message);
                return -1;
            }        
           
        }
        public int UpdateMenuByPerfil(int idPerfil)
        {
            using (var context = new GeminysEntities())
            {
                var perfil_Menu = context.Perfil_Menu.SingleOrDefault(pm => pm.idPerfil == idPerfil);

                var listaMenu = (from a in context.Perfil_Menu
                                 from menu in context.Menu
                                 where a.idPerfil == idPerfil && ((menu.idMenu == a.idMenu) || (menu.idPadre == a.idMenu))
                                 select menu).ToList();


                if (perfil_Menu != null)
                {
                    foreach (var menuItem in listaMenu)
                    {
                        perfil_Menu.idMenu = menuItem.idMenu;
                        perfil_Menu.idPerfil = idPerfil;
                    }

                    perfil_Menu = new Perfil_Menu
                    {

                    };                              
                }
                return 1;
            }
        }
        
        public int AddPerfilMenu(string strPerfilMenu, int? idPerfil)
        {
            try
            {
                using (var context = new GeminysEntities())
                {
                    context.sp_GuardaPerfilMenu(strPerfilMenu,idPerfil);
                    return 1;             
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine("{0}", e.Message);
                return -1;
            }
        }
    }
}
