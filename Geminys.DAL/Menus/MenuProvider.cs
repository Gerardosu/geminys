﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Core.Objects;
using System.Diagnostics;

namespace Geminys.DAL
{

    public class MenuProvider
    {

        public IEnumerable<Menu> GetMenus()
        {
            using (var context = new GeminysEntities())
            {
                //IList<Menu> result = ((ObjectQuery<Menu>)context.Menu.Where(x => x.activo == false)).ToList();
                IEnumerable<Menu> result = context.Menu.ToList();
                return result;
            }
        }

        /// <summary>
        /// Metodo que devuelve el menu correspondiente a un usuario con un determinado perfil
        /// </summary>
        /// <param name="idPerfil"></param>
        /// <returns></returns>
        public IEnumerable<GetAllMenuByPerfil_Result> GetMenusByPerfil(int idPerfil)
        {
            try
            {
                using (var context = new GeminysEntities())
                {                 
                    var lista = context.GetAllMenuByPerfil(idPerfil).ToList();
                    return lista;
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine("{0}", e.Message);
                return null; 
            }
        }

        /// <summary>
        /// Metodo que devuelve los datos del usuario logado y incluye el inicio de sesion en 
        /// <returns>Objeto de tipo GEMINYS.DAL.USUARIOS</returns>
        /// </summary>
        /// <returns></returns>
        public Usuarios GetUsuario ()
        {
            Usuarios usuario = null;
            try
            {
                using (var context = new GeminysEntities())
                {
                   usuario = context.Usuarios.Where(x => x.dne == Environment.UserName).FirstOrDefault();
                
                }
                
                this.RegistrarSesion(usuario.idUsuario);
                return usuario;
            }
            catch (Exception ex)
            {

                Debug.WriteLine("{0}", ex.Message);
                return null;
            }

           
        }

        /// <summary>
        /// Metodo que registra la conexion de un usuario.Si el usuario ya realizo conexion en el dia se actualiza la fecha del registro.
        /// </summary>
        /// <param name="idUsuarioLogado"></param>
        private void RegistrarSesion(int idUsuarioLogado) {

            // fecha dia actual, desde las cero horas
            DateTime dt = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
            // fecha dia actual + 1 , hasta las cero horas
            DateTime dt1 = dt.AddDays(1);

            SesionUsuarios usuario = null;
            try
            {
                using (var context = new GeminysEntities())
                {
                    usuario = context.SesionUsuarios.Where(x => x.idUsuario == idUsuarioLogado && x.horaInicio >= dt && dt1 >= dt ).FirstOrDefault();
                    // si el usuario ya se logo en dia en curso, actualizamos su registro, en caso contratrio generamos el registro con el inicio de sesion
                    if (usuario != null)
                    {
                        usuario.horaInicio = DateTime.Now;
                        context.SaveChanges();
                    }
                    else {
                        var usuarioSesion = context.SesionUsuarios;
                        usuarioSesion.Add(new SesionUsuarios { idUsuario = idUsuarioLogado, horaInicio = DateTime.Now, horaFin = null });
                        context.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("{0}", ex.Message);
            }
        }


        /// <summary>
        /// Metodo que devuelve la noticia que debe aparecer en la cabecera.
        /// Actualmente solo mostramos la que esta activa para la portada.
        /// <return>Objeto de tipo GEMINYS.DAL.NOTICIAS</return>
        /// </summary>
        /// <returns></returns>
        public Noticias GetNoticias() {

            using (var context = new GeminysEntities())
            {
                Noticias noticias = context.Noticias.Where(n => n.activoPortada == true).FirstOrDefault();
                Debug.WriteLine("{0}{1}", noticias.activoPortada,noticias.descripcion);
                return noticias;
            }
        }

        /// <summary>
        /// Metodo que devuelve un listado con el personal de guardia
        /// <returns>Lista con objetos de tipo GEMINYS.DAL.DIRECTORIO_PERSONAL</returns>
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Directorio_Personal> GetPersonalGuardia() {

            using (var context = new GeminysEntities())
            {
                // IEnumerable<Directorio_Personal> listadoGuardias = context.Directorio_Personal.ToList();
                IEnumerable<Directorio_Personal> listadoGuardias = context.Directorio_Personal.Where(n => n.guardias == true && n.activo == true).ToList();
          
                return listadoGuardias;
            }          
        }

        /// <summary>
        /// Metodo que devuelve todo el menu y submenu
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Menu> GetAllMenus()
        {
            try
            {
                using (var context = new GeminysEntities())
                {
                    //var lista = (from a in context.Menu 
                    //             from menu in context.Menu
                    //             where  ((menu.idPadre == a.idMenu))
                    //             select menu).ToList();
                    var lista = (from m in context.Menu
                                 join s in context.Menu on m.idMenu equals s.idMenu into AllMenu
                                 from menu in AllMenu.DefaultIfEmpty() 
                                 orderby menu.idPadre, menu.orden           
                                 select menu).ToList();
                    return lista;
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine("{0}{1}", e.Message);
                return null;
            }
        }

        public IEnumerable<Perfil> GetPerfiles()
        {
            using (var context = new GeminysEntities())
            {
                IEnumerable<Perfil> perfiles = context.Perfil.Where(x => x.activo == true).ToList();              
                return perfiles;
            }
        }

    }

}
