﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Geminys.DAL.Combos
{
    /// <summary>
    /// Clase estatica de negocio para rellenar los distintos combos que componen la aplicación
    /// </summary>
   public static class ComboProvider
    {
        /// <summary>
        /// Metodo que devuelve la un listado con las empresas asociadas a Metro
        /// </summary>
        /// <returns></returns>
        public static List<Empresas> getListaEmpresas()
        {

            List<Empresas> listaEmpresas = null;

            try
            {
                using (var context = new GeminysEntities())
                {
                    listaEmpresas = context.Empresas.Where(x => x.activo == true).ToList();

                }
                return listaEmpresas;
            }

            catch (Exception ex)
            {
                Debug.WriteLine("{0}{1}", ex.Message);
                return null;
            }

        }
        /// <summary>
        /// Listado de Perfiles
        /// </summary>
        /// <returns></returns>
        public static List<Perfil> getListaPerfiles() {

            List<Perfil> listaPerfil = null;
            try
            {
                using (var context = new GeminysEntities())
                {
                    listaPerfil = context.Perfil.Where(x => x.activo == true).ToList();

                }
                return listaPerfil;
            }

            catch (Exception ex)
            {
                Debug.WriteLine("{0}{1}", ex.Message);
                return null;
            }
        }

    }
}
