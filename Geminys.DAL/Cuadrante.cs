//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Geminys.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class Cuadrante
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Cuadrante()
        {
            this.Cuadrante_Horario = new HashSet<Cuadrante_Horario>();
        }
    
        public int idCuadrante { get; set; }
        public System.DateTime fechaSistema { get; set; }
        public string dne { get; set; }
        public int orden { get; set; }
        public System.DateTime fechaInicio { get; set; }
        public System.DateTime fechaFin { get; set; }
        public int idLote { get; set; }
        public Nullable<int> idZona { get; set; }
        public int idLinea { get; set; }
        public int idCodigo { get; set; }
        public int idServicio { get; set; }
        public int idVestuario { get; set; }
        public int idCuadranteTipo { get; set; }
        public int idEmpresa { get; set; }
        public Nullable<decimal> horas { get; set; }
        public Nullable<bool> ultimaModificacion { get; set; }
    
        public virtual Cuadrante_Codigo Cuadrante_Codigo { get; set; }
        public virtual Cuadrante_Servicios Cuadrante_Servicios { get; set; }
        public virtual Cuadrante_Tipo Cuadrante_Tipo { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Cuadrante_Horario> Cuadrante_Horario { get; set; }
        public virtual Lotes Lotes { get; set; }
        public virtual Zonas Zonas { get; set; }
    }
}
