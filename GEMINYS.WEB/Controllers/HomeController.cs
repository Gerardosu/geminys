﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Geminys.DAL;
using GEMINYS.WEB.Models;


namespace GEMINYS.WEB.Controllers
{
    public class HomeController : Controller
    {
        /// <summary>
        /// Accion que devuelve los datos de la cabera
        /// <return>El modelo contiene la informacion del usuario, noticia de portada, personal de guardia y menu del usuario</return>
        /// </summary>
        /// <returns>MainHeaderModel</returns>
        [HttpGet]
        public JsonResult GetMenu()
        {
            MainHeaderModel modelHeader = new MainHeaderModel();
            return Json(modelHeader, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Index()
        {
            ViewBag.Miga = "Pagina maestra";
            return View();
        }

        //public ActionResult About()
        //{
        //    ViewBag.Message = "Your application description page.";
        //    return View();
        //}
    }
}