﻿using GEMINYS.WEB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GEMINYS.WEB.Controllers
{
    public class IncidenciasController : Controller
    {
        // GET: Incidencias
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Incidencias()
        {
            if (System.Web.HttpContext.Current.Cache["MenuUser"] == null)
            {
                MainHeaderModel modelo = new MainHeaderModel();
                System.Web.HttpContext.Current.Cache["MenuUser"] = modelo;
            }

            var lista = ((MainHeaderModel)System.Web.HttpContext.Current.Cache["MenuUser"]);
            ViewBag.Miga = "Incidencias";
            return View(lista);
        }

        public ActionResult BajadaVia() {

            return View();
        }

        public ActionResult Encierres()
        {

            return View();
        }
    }
}