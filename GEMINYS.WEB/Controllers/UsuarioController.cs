﻿using Geminys.BLL;
using Geminys.BLL.Menus;
using GEMINYS.WEB.Models;
using GEMINYS.WEB.Models.BackOffice;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GEMINYS.WEB.Controllers
{
    public class UsuarioController : Controller
    {
      
        [HttpPost]
        public ActionResult MantenimientoUsuario(UsuarioModel modeloUsuario)
        {

            return View(modeloUsuario);
        }

        [HttpGet]
        public ActionResult MantenimientoUsuario()
        {
            UsuarioModel modeloUsuario = new UsuarioModel();
            UsuarioBLL objUsuario = new UsuarioBLL();
            modeloUsuario.listadoUsuarios =  modeloUsuario.getListaUsuarios(objUsuario.GetListaUsuarios());
            return View(modeloUsuario);
        }

        /// <summary>
        /// Accion que retorna los usuarios segun el criterio de busqueda
        /// </summary>
        /// <param name="modeloUsuario"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult BuscarUsuarios(UsuarioModel busquedaUsuario)
        {
    
            UsuarioBLL objUsuario = new UsuarioBLL();
            busquedaUsuario.listadoUsuarios = busquedaUsuario.getListaUsuarios(objUsuario.FindUsuarios(busquedaUsuario.nombre,busquedaUsuario.apellido1,busquedaUsuario.apellido2, busquedaUsuario.dne,busquedaUsuario.activo));
            return View("MantenimientoUsuario", busquedaUsuario);
        }
        /// <summary>
        /// Accion que gestiona el alta de un usuario
        /// </summary>
        /// <param name="nombre"></param>
        /// <param name="apellido1"></param>
        /// <param name="apellido2"></param>
        /// <param name="dne"></param>
        /// <param name="tip"></param>
        /// <param name="empresa"></param>
        /// <param name="perfil"></param>
        /// <param name="telefono"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult AltaUsuario(string nombre, string apellido1,string apellido2,string dne,string tip,string empresa,string perfil,string telefono)
        {
            bool resultadoAlta = UsuarioBLL.AltaUsuario(nombre, apellido1, apellido2, dne, tip, empresa, perfil, telefono);
            return Json(resultadoAlta, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Accion que gestiona la modificacion de un usuario
        /// </summary>
        /// <param name="nombre"></param>
        /// <param name="apellido1"></param>
        /// <param name="apellido2"></param>
        /// <param name="dne"></param>
        /// <param name="tip"></param>
        /// <param name="empresa"></param>
        /// <param name="perfil"></param>
        /// <param name="telefono"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult ModificacionUsuario(string nombre, string apellido1, string apellido2, string dne, string tip, string empresa, string perfil, string telefono)
        {
            bool resultadoModificacion = UsuarioBLL.ModificacionUsuario(nombre, apellido1, apellido2, dne, tip, empresa, perfil, telefono);
            return Json(resultadoModificacion, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Metodo que activa o desactiva un usuario
        /// </summary>
        /// <param name="idUsuario"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult ActivarDesactivarUsuario(string idUsuario,string operacion)
        {
            bool resultadoOperacion = UsuarioBLL.ActivarDesactivarUsuario(idUsuario,operacion);
            return Json(resultadoOperacion, JsonRequestBehavior.AllowGet);
        }



        /// <summary>
        /// Accion que retorna el listado de empresas asociadas a METRO
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public JsonResult ListaEmpresas()
        {
            EmpresaModel modeloEmpresa = new EmpresaModel();
            modeloEmpresa.listaEmpresas = modeloEmpresa.getListaEmpresas(CombosBLL.getEmpresas());
            return Json(modeloEmpresa.listaEmpresas, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Accion que retorna el listado de perfiles asociados a usuarios
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public JsonResult ListaPerfiles()
        {
            PerfilModel modeloPerfil = new PerfilModel();
            modeloPerfil.listaPerfiles = modeloPerfil.getListaPerfil(CombosBLL.getPerfiles());
            return Json(modeloPerfil.listaPerfiles, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Accion que retorna un listado con los usuarios conectados
        /// </summary>
        /// <returns></returns>
                 
        [HttpGet]
        public ActionResult UsuariosConectados() {

            UsuarioConectadoModel modeloUsuarioConectado = new UsuarioConectadoModel();
            UsuarioBLL objUsuario = new UsuarioBLL();
            modeloUsuarioConectado.listadoUsuarios = modeloUsuarioConectado.findUsuariosConectados(objUsuario.FindUsuariosConectados(string.Empty));
            return View(modeloUsuarioConectado);

        }
        /// <summary>
        ///  
        /// </summary>
        /// <param name="fecha"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult GetUsuariosConectados(string fecha)
        {
            UsuarioConectadoModel modeloUsuarioConectado = new UsuarioConectadoModel();
            UsuarioBLL objUsuario = new UsuarioBLL();
            modeloUsuarioConectado.listadoUsuarios = modeloUsuarioConectado.findUsuariosConectados(objUsuario.FindUsuariosConectados(fecha));
            return Json(modeloUsuarioConectado, JsonRequestBehavior.AllowGet);
        }


    }
}