﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GEMINYS.WEB.Models.BackOffice;
using GEMINYS.WEB.Models;
using Geminys.BLL;

namespace GEMINYS.WEB.Controllers
{
    public class PerfilUsuarioController : Controller
    {
        [HttpGet]
        public ActionResult AltaPerfil(string nombre)
        {
            //PerfilUsuario.
            PerfilBLL obj = new PerfilBLL();
            int resultado = obj.AddPerfil(nombre);
           
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Perfil()
        {
            //PerfilModel modeloPerfil = new PerfilModel();
            return View();
        }

        [HttpGet]
        public JsonResult GetMenuPerfiles(string strPerfil)
        {
            PerfilModel modelPerfil = new PerfilModel();
            MenuItemBLL objMenuItemBLL = new MenuItemBLL();
            // Si es nulo, obtiene toda la lista de menu
            if (string.IsNullOrEmpty(strPerfil))
            {
                modelPerfil.listadoMenuByPerfil = modelPerfil.getListaMenuByPerfil(0);
                //modelPerfil.listadoMenuPerfil = modelPerfil.getListaMenu(objMenuItemBLL.GetAllMenu());             
               // modelPerfil.listadoPerfiles = modelPerfil.getListaPerfiles();
            }
            else
            {
                //solo lista de menu por Perfil 
                modelPerfil.listadoMenuByPerfil = modelPerfil.getListaMenuByPerfil(Convert.ToInt32(strPerfil));
            }  
            return Json(modelPerfil, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AltaPerfilMenu(string strPerfilMenu, string strPerfil)
        {
            //PerfilUsuario.
            PerfilBLL obj = new PerfilBLL();
            int resultado = obj.AddPerfilMenu(strPerfilMenu, Convert.ToInt32(strPerfil));

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

    }
}