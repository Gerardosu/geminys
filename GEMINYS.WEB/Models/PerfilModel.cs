﻿using Geminys.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GEMINYS.WEB.Models
{
    public class PerfilModel
    {
        public int idPerfil { get; set; }
        public string nombrePerfil { get; set; }
        public bool activo { get; set; }

        public List<PerfilModel> listaPerfiles { get; set; }

        /// <summary>
        /// Metodo que devuelve un listado con los perfiles disponibles en Metro.
        /// </summary>
        /// <param name="listadoPerfiles"></param>
        /// <returns></returns>
        public List<PerfilModel> getListaPerfil(IEnumerable<Perfil> listadoPerfiles)
        {
            List<PerfilModel> lista = new List<PerfilModel>();
            foreach (var perfil in listadoPerfiles)
            {
                PerfilModel objetoPerfil = new PerfilModel();
                objetoPerfil.nombrePerfil = perfil.nombrePerfil;
                objetoPerfil.idPerfil = perfil.idPerfil;
                lista.Add(objetoPerfil);
            }
            return lista;
        }
    }
}