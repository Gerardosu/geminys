﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Geminys.BLL.Menus;
using Geminys.BLL;
using Geminys.DAL;
using System.ComponentModel.DataAnnotations;

namespace GEMINYS.WEB.Models
{
    public class UsuarioModel
    {
        public int idUsuario { get; set; }
        public string nombre { get; set; }
        public string apellido1 { get; set; }
        public string apellido2 { get; set; }
        public string dne { get; set; }
        public int idEmpresa { get; set; }
        public string nombreEmpresa { get; set; }
        public int idPerfil { get; set; }
        public string nombrePerfil { get; set; }
        public string tip { get; set; }
        public string telefono { get; set; }
        public Nullable<bool> activo { get; set; }


        public IEnumerable<UsuarioModel> listadoUsuarios {get;set;}


        public List<UsuarioModel> getListaUsuarios(IEnumerable<Usuarios> listadoUsuarios)
        {

            List<UsuarioModel> lista = new List<UsuarioModel>();

            foreach (var usuario in listadoUsuarios)
            {
                UsuarioModel us = new UsuarioModel();
                us.idUsuario = usuario.idUsuario;
                us.nombre = usuario.nombre;
                us.idEmpresa = usuario.idEmpresa;
                us.idPerfil = usuario.idPerfil;
                us.apellido1 = usuario.apellido1;
                us.apellido2 = usuario.apellido2;
                us.tip = usuario.tip;
                us.dne = usuario.dne;
                us.telefono = usuario.telefono;
                us.nombrePerfil = usuario.Perfil.nombrePerfil;
                us.nombreEmpresa = usuario.Empresas.empresa;
                us.activo = usuario.activo;
                lista.Add(us);
            }

            return lista;
        }

        /// <summary>
        /// Metodo que devuelve un listado de usuarios segun un critero de busqueda
        /// </summary>
        /// <param name="listadoUsuarios"></param>
        /// <returns></returns>
        public List<UsuarioModel> findUsuarios(IEnumerable<Usuarios> listadoUsuarios)
        {

            List<UsuarioModel> lista = new List<UsuarioModel>();

            foreach (var usuario in listadoUsuarios)
            {
                UsuarioModel us = new UsuarioModel();
                us.nombre = usuario.nombre;
                us.idEmpresa = usuario.idEmpresa;
                us.idPerfil = usuario.idPerfil;
                us.apellido1 = usuario.apellido1;
                us.apellido2 = usuario.apellido2;
                us.tip = usuario.tip;
                us.dne = usuario.dne;
                lista.Add(us);
            }

            return lista;
        }
       

    }
}