﻿using Geminys.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GEMINYS.WEB.Models
{
    public class EmpresaModel
    {

        public int idEmpresa { get; set; }
        public string empresa { get; set; }
        public string telefono { get; set; }
        public string mailEnvio { get; set; }
        public string nombreCarpeta { get; set; }
        public bool activo { get; set; }

        public List<EmpresaModel> listaEmpresas { get; set; }


        public List<EmpresaModel> getListaEmpresas(IEnumerable<Empresas> listadoEmpresas){


            List<EmpresaModel> lista = new List<EmpresaModel>();

            foreach (var empresa in listadoEmpresas)
            {
                EmpresaModel objetoEmpresa = new EmpresaModel();
                objetoEmpresa.empresa = empresa.empresa;
                objetoEmpresa.idEmpresa = empresa.idEmpresa;
                lista.Add(objetoEmpresa);
            }

            return lista;

        }
    }
}