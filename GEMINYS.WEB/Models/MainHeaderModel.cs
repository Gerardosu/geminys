﻿using Geminys.BLL.Menus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Geminys.BLL;
using Geminys.DAL;

namespace GEMINYS.WEB.Models
{
     /// <summary>
     /// Clase mediante la cual vamos a obtener los datos necesarios para rellenar la cabecera la web
     /// </summary>
    public class MainHeaderModel
    {
        public IEnumerable<MenuItemModel> MenuItems { get; set; }
        public IEnumerable<PersonalGuardiaModel> Guardias { get; set; }
        public NoticiaModel Noticias { get; set; }
        public UsuarioModel   Usuario { get ; set; }

        /// <summary>
        /// Constructor de la clase.Desde el recuperamos la informacion DAL para su envio.
        /// </summary>
        public MainHeaderModel() {

            this.Usuario = this.getUsuarioModel();
            this.Noticias = this.getNoticiaModel();
            this.Guardias = this.getListaPersonalGuardia();        
            this.MenuItems = this.getListaMenuByPerfil(Usuario.idPerfil);
        }

        /// <summary>
        /// Metodo que devuelve informacion sobre el usuario logado
        /// </summary>
        /// <returns></returns>
        private UsuarioModel getUsuarioModel()
        {
            UsuarioModel usuario = new UsuarioModel();
            UsuarioBLL objUsuario = new UsuarioBLL();
            Usuarios objUsuarioDAL = objUsuario.GetUsuario();

            usuario.idUsuario = objUsuarioDAL.idUsuario;
            usuario.nombre = objUsuarioDAL.nombre;
            usuario.apellido1 = objUsuarioDAL.apellido1;
            usuario.apellido2 = objUsuarioDAL.apellido2;
            usuario.dne = objUsuarioDAL.dne;
            usuario.idEmpresa = objUsuarioDAL.idEmpresa;
            usuario.idPerfil = objUsuarioDAL.idPerfil;

            return usuario;
            
        }

        /// <summary>
        /// Metodo que devuelve la noticia activa que va a figurar en la portada
        /// </summary>
        /// <returns></returns>
        private NoticiaModel  getNoticiaModel()
        {
            NoticiaModel noticia = new NoticiaModel();
            NoticiasBLL objNoticias = new NoticiasBLL();
            Noticias objNoticiasDAL = objNoticias.getNoticias();
            noticia.idNoticia = objNoticiasDAL.idNoticia;
            noticia.descripcion = objNoticiasDAL.descripcion;
            noticia.titulo = objNoticiasDAL.titulo;
            return noticia;
        }


        /// <summary>
        /// Metodo que retorna un listado con el personal de guardia
        /// </summary>
        /// <returns></returns>
        private List<PersonalGuardiaModel> getListaPersonalGuardia()
        {
            GuardiaBLL objGuardiasBLL = new GuardiaBLL();
            IEnumerable<Directorio_Personal> objGuardiasDAL = objGuardiasBLL.GetPersonalGuardia();

            List<PersonalGuardiaModel> listaGuardia = new List<PersonalGuardiaModel>();

            foreach (var persona in objGuardiasDAL)
            {
                PersonalGuardiaModel pg = new PersonalGuardiaModel();
                pg.nombre = persona.nombre;
                pg.movilCorporativo = persona.movilCorporativo;
                pg.puesto = persona.puesto;
                listaGuardia.Add(pg);
            }
            return listaGuardia;
        }

        /// <summary>
        /// Metodo que retorna un listado del menú de acuerdo al perfil de usuario asignado.
        /// </summary>
        /// <returns></returns>
        private List<MenuItemModel> getListaMenuByPerfil(int idPerfil)
        {
            MenuItemBLL objMenuUsuario = new MenuItemBLL();
            IEnumerable<GetAllMenuByPerfil_Result> objMenuUsuarioDAL = objMenuUsuario.GetMenuUsuario(idPerfil);

            List<MenuItemModel> listaMenu = new List<MenuItemModel>();

            foreach (var menu in objMenuUsuarioDAL)
            {
                MenuItemModel menuUsuario = new MenuItemModel();
                menuUsuario.idMenu = menu.idmenu;
                menuUsuario.idPadre = menu.idPadre;
                menuUsuario.nombreMenu = menu.nombreMenu;
                menuUsuario.claseImagen = menu.claseImagen;
                menuUsuario.Link = menu.link;
                menuUsuario.atributo = menu.atributo;
                listaMenu.Add(menuUsuario);

            }
            return listaMenu;
        }
    }
}