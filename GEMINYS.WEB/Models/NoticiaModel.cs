﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Geminys.BLL;
using Geminys.DAL;

namespace GEMINYS.WEB.Models
{
    public class NoticiaModel
    {
        public int idNoticia { get; set; }
        public string dne { get; set; }
        public System.DateTime fechaInicio { get; set; }
        public System.DateTime fechaFin { get; set; }
        public string titulo { get; set; }
        public string descripcion { get; set; }
        public Nullable<bool> resaltado { get; set; }
        public Nullable<bool> activoPortada { get; set; }
        public Nullable<bool> activoInformeDiario { get; set; }
        public Nullable<bool> titular { get; set; }
        public Nullable<bool> activo { get; set; }
       
    }
}