﻿using Geminys.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GEMINYS.WEB.Models
{
    public class UsuarioConectadoModel
    {
        public int idSesionUsuario { get; set; }
        public int idUsuario { get; set; }
        public System.DateTime fechaInicio { get; set; }
        public Nullable<System.DateTime> fechaFin { get; set; }

        public int horaInicio { get; set; }

        public int minutoInicio { get; set; }

        public virtual UsuarioModel Usuario { get; set; }

        public IEnumerable<UsuarioConectadoModel> listadoUsuarios { get; set; }

        public List<UsuarioConectadoModel> findUsuariosConectados(IEnumerable<SesionUsuarios> listadoUsuarios)
        {

            List<UsuarioConectadoModel> lista = new List<UsuarioConectadoModel>();

            foreach (var usuario in listadoUsuarios)
            {
                UsuarioConectadoModel us = new UsuarioConectadoModel();
                UsuarioModel datosUsuarios = new UsuarioModel();
                us.fechaInicio = usuario.horaInicio;
                us.horaInicio = usuario.horaInicio.Hour;
                us.minutoInicio = usuario.horaInicio.Minute;
                us.fechaFin = usuario.horaInicio;
                us.idSesionUsuario = usuario.idSesionUsuario;
                us.idUsuario = usuario.idUsuario;
                // se mapean los datos del usuario
                datosUsuarios.nombre = usuario.Usuarios.nombre;
                datosUsuarios.idEmpresa = usuario.Usuarios.idEmpresa;
                datosUsuarios.idPerfil = usuario.Usuarios.idPerfil;
                datosUsuarios.apellido1 = usuario.Usuarios.apellido1;
                datosUsuarios.apellido2 = usuario.Usuarios.apellido2;
                datosUsuarios.tip = usuario.Usuarios.tip;
                datosUsuarios.dne = usuario.Usuarios.dne;
                // se añaden los datos del usuario
                us.Usuario = datosUsuarios;
                lista.Add(us);
            }

            return lista;
        }
    }

   
}