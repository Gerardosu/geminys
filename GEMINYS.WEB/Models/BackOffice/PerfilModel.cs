﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Geminys.BLL;
using Geminys.DAL;

namespace GEMINYS.WEB.Models.BackOffice
{
    public class PerfilModel
    {
        public int idPerfil { get; set; }
        public string nombrePerfil { get; set; }
        public bool activo { get; set; }

        public List<PerfilModel> listaPerfiles { get; set; }
       // public List<PerfilModel> listadoPerfiles { get; set; }

        public List<MenuItemModel> listadoMenuPerfil { get; set; }

        public List<MenuItemModel> listadoMenuByPerfil { get; set; }


        public List<MenuItemModel>getListaMenu(IEnumerable<Menu> listaMenu)
        {
          
            this. listadoMenuPerfil = new List<MenuItemModel>();
           
            foreach (var menu in listaMenu)
            {
                MenuItemModel menuUsuario = new MenuItemModel();
                menuUsuario.idMenu = menu.idMenu;
                menuUsuario.idPadre = menu.idPadre;
                menuUsuario.nombreMenu = menu.nombreMenu;            
                listadoMenuPerfil.Add(menuUsuario);

            }
            return listadoMenuPerfil;
        }


        //public List<PerfilModel>getListaPerfiles()
        //{
        //    PerfilBLL objPerfilBLL = new PerfilBLL();
        //    IEnumerable<Perfil> objPerfilDAL = objPerfilBLL.GetAllPerfiles();

        //    this.listadoPerfiles = new List<PerfilModel>();

        //    foreach (var perfil in objPerfilDAL)
        //    {
        //        PerfilModel perfilUsuario = new PerfilModel();
        //        perfilUsuario.idPerfil = perfil.idPerfil;
        //        perfilUsuario.nombrePerfil = perfil.nombrePerfil;
        //        perfilUsuario.activo = perfil.activo;
        //        this.listadoPerfiles.Add(perfilUsuario);
        //    }
        //    return listadoPerfiles;
        //}

        /// <summary>
        /// Metodo que devuelve un listado con los perfiles disponibles en Metro.
        /// </summary>
        /// <param name="listadoPerfiles"></param>
        /// <returns></returns>
        public List<PerfilModel> getListaPerfil(IEnumerable<Perfil> listadoPerfiles)
        {
            List<PerfilModel> lista = new List<PerfilModel>();
            foreach (var perfil in listadoPerfiles)
            {
                PerfilModel objetoPerfil = new PerfilModel();
                objetoPerfil.nombrePerfil = perfil.nombrePerfil;
                objetoPerfil.idPerfil = perfil.idPerfil;
                lista.Add(objetoPerfil);
            }
            return lista;
        }
        public List<MenuItemModel> getListaMenuByPerfil(int idPerfil)
        {
           
            MenuItemBLL objMenuUsuario = new MenuItemBLL();
            IEnumerable<GetAllMenuByPerfil_Result> objMenuUsuarioDAL = objMenuUsuario.GetMenuUsuario(idPerfil);
     
            this.listadoMenuByPerfil = new List<MenuItemModel>();
            foreach (var menu in objMenuUsuarioDAL)
            {
                MenuItemModel menuUsuario = new MenuItemModel();
                menuUsuario.idMenu = menu.idmenu;
                menuUsuario.idPadre = menu.idPadre;
                menuUsuario.nombreMenu = menu.nombreMenu;
                menuUsuario.claseImagen = menu.claseImagen;
                menuUsuario.Link = menu.link;
                menuUsuario.atributo = menu.atributo;
                listadoMenuByPerfil.Add(menuUsuario);

            }
            return listadoMenuByPerfil;
        }

    }
}                     