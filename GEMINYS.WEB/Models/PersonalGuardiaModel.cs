﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GEMINYS.WEB.Models
{
    public class PersonalGuardiaModel
    {
        public int idDirectorioPersonal { get; set; }
        public string nombre { get; set; }
        public int idEmpresa { get; set; }
        public string servicio { get; set; }
        public string puesto { get; set; }
        public string email { get; set; }
        public string fijo { get; set; }
        public string movilCorporativo { get; set; }
        public string movilLargo { get; set; }
        public string comentarios { get; set; }
        public Nullable<bool> guardias { get; set; }
        public Nullable<bool> activo { get; set; }
    }
}