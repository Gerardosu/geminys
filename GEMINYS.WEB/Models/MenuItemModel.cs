﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Geminys.DAL;
using System.Data.Entity;
using Geminys.BLL;

namespace GEMINYS.WEB.Models
{
    public class MenuItemModel
    {
        public int idMenu { get; set; }
        public string nombreMenu { get; set; }
        public int? idPadre { get; set; }
        public string claseImagen { get; set; }
        public int orden { get; set; }
        public string Link { get; set; }
        public bool activo { get; set; }
        public string atributo { get; set; }
     
    }

    
}