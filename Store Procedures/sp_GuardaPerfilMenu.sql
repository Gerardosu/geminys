
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Gerardo Su>
-- Create date: <20-03-2018>
-- Description:	<Agrega los menus de acuerdo al perfil>
-- =============================================
ALTER PROCEDURE sp_GuardaPerfilMenu --'|1|19|20|21|22|23|31|24',3
	@cadena_menu varchar(MAX),
	@idPerfil int
AS
BEGIN

	SET NOCOUNT ON;



Declare @MenuID varchar(20) = null
DECLARE @Counter int = 0; 

--Borra el menu relacionado al perfil
	DELETE Perfil_Menu 
	WHERE idPerfil = @idPerfil 

WHILE LEN(@cadena_menu) > 0
BEGIN
    IF PATINDEX('%|%', @cadena_menu) > 0
    BEGIN
        SET @MenuID = SUBSTRING(@cadena_menu, 0, PATINDEX('%|%', @cadena_menu))
  
        SET @cadena_menu = SUBSTRING(@cadena_menu, LEN(@MenuID + '|') + 1, LEN(@cadena_menu))
    END
    ELSE
    BEGIN
        SET @MenuID = @cadena_menu
        SET @cadena_menu = NULL      
    END

	
	--Inserta la relacion de Perfil/Menu
	INSERT INTO Perfil_Menu 
	VALUES (@idPerfil,@MenuID) 


	SET @Counter = @Counter + @@ROWCOUNT

END

--SELECT @Counter

END

GO

