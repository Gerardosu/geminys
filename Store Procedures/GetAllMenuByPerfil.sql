USE [Geminys4]
GO
/****** Object:  StoredProcedure [dbo].[GetAllMenuByPerfil]    Script Date: 20/03/2018 19:44:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Gerardo Su>
-- Create date: <16-03-2018>
-- Description:	<Devuelve todo el menu y submenu por usuario>
-- =============================================
ALTER PROCEDURE [dbo].[GetAllMenuByPerfil]  
	@idPerfil int
	
AS
BEGIN

	SET NOCOUNT ON;

	
	SELECT idmenu,nombreMenu,idPadre,claseImagen,orden,link,activo, '' atributo
	FROM menu 
	WHERE idMenu not in (SELECT m.idmenu 
							FROM Perfil_Menu pm inner join menu m ON pm.idMenu = m.idMenu 
							WHERE pm.idPerfil = @idPerfil and m.activo = 1)
	UNION 
	SELECT m.idmenu,m.nombreMenu,m.idPadre,m.claseImagen,m.orden,m.link,m.activo,  'checked=''true''' atributo
	FROM Perfil_Menu pm inner join menu m ON pm.idMenu = m.idMenu 
	WHERE pm.idPerfil = @idPerfil and m.activo = 1
	ORDER BY idPadre,orden


END

