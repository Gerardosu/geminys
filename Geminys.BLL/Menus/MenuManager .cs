﻿using System;
using System.Collections.Generic;
using System.Linq;
using Geminys.DAL;

namespace Geminys.BLL.Menus
{
    public class MenuManager
    {

        public IEnumerable<Menu> GetAllMenus()
        {
            MenuProvider newprovider = new MenuProvider();
            return newprovider.GetMenus(); 
        }      
        /* Metodo para obtener los datos del usuario logado */
        public Usuarios GetUsuario()
        {
            MenuProvider newprovider = new MenuProvider();
            var usuario = newprovider.GetUsuario();
            return usuario;
        }

   
    }
}
