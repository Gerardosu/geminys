﻿using Geminys.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Geminys.BLL
{
    public class NoticiasBLL
    {
        public int idNoticia { get; set; }
        public string dne { get; set; }
        public System.DateTime fechaInicio { get; set; }
        public System.DateTime fechaFin { get; set; }
        public string titulo { get; set; }
        public string descripcion { get; set; }
        public Nullable<bool> resaltado { get; set; }
        public Nullable<bool> activoPortada { get; set; }
        public Nullable<bool> activoInformeDiario { get; set; }
        public Nullable<bool> titular { get; set; }
        public Nullable<bool> activo { get; set; }

        /// <summary>
        /// Constructor vacio
        /// </summary>
        public NoticiasBLL() {

        }
        /// <summary>
        /// Metodo que retorna un objeto NOTICIAS,en este caso la noticia que este activa para ser mostrada en la cabecera
        /// <return></return>
        /// </summary>
        /// <returns></returns>
        public Noticias getNoticias() {

            MenuProvider newprovider = new MenuProvider();
            var noticiasDAL = newprovider.GetNoticias();
            this.idNoticia = noticiasDAL.idNoticia;
            this.titulo = noticiasDAL.titulo;
            this.descripcion = noticiasDAL.descripcion;
            return noticiasDAL;

        }
    }
}
