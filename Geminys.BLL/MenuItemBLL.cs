﻿using Geminys.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Geminys.BLL
{
   public class MenuItemBLL
    {

        public int idMenu { get; set; }
        public string nombreMenu { get; set; }
        public int? idPadre { get; set; }
        public string claseImagen { get; set; }
        public int orden { get; set; }
        public string Link { get; set; }
        public bool activo { get; set; }

        /// <summary>
        /// Constructor vacio
        /// </summary>
        public MenuItemBLL() { }
        /// <summary>
        /// Metodo que retorna el menut de un usuario segun el perfil que tenga
        /// </summary>
        /// <returns></returns>
        public IEnumerable<GetAllMenuByPerfil_Result> GetMenuUsuario(int idPerfil) {
            MenuProvider newprovider = new MenuProvider();
            var menuUsuarioDAL = newprovider.GetMenusByPerfil(idPerfil);
            return menuUsuarioDAL;
        }

        public IEnumerable<Menu> GetAllMenu()
        {
            MenuProvider newprovider = new MenuProvider();
            var menuUsuarioDAL = newprovider.GetAllMenus();
            return menuUsuarioDAL;
        }
    }
}
