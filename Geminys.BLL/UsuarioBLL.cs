﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Geminys.DAL;
using Geminys.DAL.Users;
using System.Diagnostics;

namespace Geminys.BLL
{
    public class UsuarioBLL
    {
        public int idUsuario { get; set; }
        public string nombre { get; set; }
        public string apellido1 { get; set; }
        public string apellido2 { get; set; }
        public string dne { get; set; }
        public int idEmpresa { get; set; }

        /// <summary>
        /// Metodo que va a realizar la consulta que devuelve los datos con información referente al usuario logado
        /// <return>Devuelve un objeto de tipo GEMINYS.DAL.USUARIOS</return>
        /// </summary>
        /// <returns></returns>
        public Usuarios GetUsuario()
        {
            MenuProvider newprovider = new MenuProvider();
            var usuarioDAL = newprovider.GetUsuario();
            this.idUsuario = usuarioDAL.idUsuario;
            this.nombre = usuarioDAL.nombre;
            this.apellido1 = usuarioDAL.apellido1;
            this.apellido2 = usuarioDAL.apellido2;
            this.dne = usuarioDAL.dne;
            this.idEmpresa = usuarioDAL.idEmpresa;
            return usuarioDAL;
        }
        /// <summary>
        /// Metodo que devuelve todos los usuarios dados de alta y activos 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Usuarios> GetListaUsuarios() {

            UsersProvider userProvider = new UsersProvider();
            var listaUsuarioDAL = userProvider.GetListadoUsuarios();
            return listaUsuarioDAL;
        }

        /// <summary>
        /// Metodo que devuelve un listado de usuarios según un criterio
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Usuarios> FindUsuarios(string nombre,string apellido1, string apellido2, string dne, bool? activo)
        {
            IEnumerable<Usuarios> listaUsuarioDAL = null;

            UsersProvider userProvider = new UsersProvider();
            if (nombre == null && apellido1 == null && apellido2 ==null && dne == null && activo == null)
            {
                 listaUsuarioDAL = userProvider.GetListadoUsuarios();
            }
            else {
                 listaUsuarioDAL = userProvider.FindUsuarios(nombre, apellido1, apellido2,dne,activo);
            }
            
            return listaUsuarioDAL;
        }

        /// <summary>
        /// Metodo que devuelve los usuarios conectados en una fecha determinada
        /// </summary>
        /// <param name="fecha"></param>
        /// <returns></returns>
        public IEnumerable<SesionUsuarios> FindUsuariosConectados(String fecha)
        {
            IEnumerable<SesionUsuarios> listaUsuarioDAL = null;
            UsersProvider userProvider = new UsersProvider();

            if (string.IsNullOrEmpty(fecha))
            {
                listaUsuarioDAL = userProvider.FindUsuariosConectados(DateTime.Now.ToString("yyyy-MM-dd"));
            }
            else {

                listaUsuarioDAL = userProvider.FindUsuariosConectados(DateTime.Parse(fecha).ToString("yyyy-MM-dd"));
            }
            return listaUsuarioDAL;
        }

        /// <summary>
        /// Metodo para dar de alta un nuevo usuario en la aplicacion.
        /// </summary>
        /// <param name="nombreAlta"></param>
        /// <param name="apellido1Alta"></param>
        /// <param name="apellido2Alta"></param>
        /// <param name="dneAlta"></param>
        /// <param name="tipAlta"></param>
        /// <param name="empresaAlta"></param>
        /// <param name="perfilAlta"></param>
        /// <param name="estadoAlta"></param>
        /// <returns></returns>
        public static bool AltaUsuario(string nombre, string apellido1, string apellido2, string dne, string tip, string empresa, string perfil, string telefono)
        {
            try
            {
               bool resultadoAlta =  UsersProvider.AltaUsuario(nombre, apellido1, apellido2, dne, tip, empresa, perfil, telefono);
               return resultadoAlta;
            }
            catch (Exception ex)
            {

                Debug.WriteLine("{0}{1}", ex.Message);
                return false;
            }
        }
        /// <summary>
        /// Metodo que modifica la información de un determinado usuario
        /// </summary>
        /// <param name="nombre"></param>
        /// <param name="apellido1"></param>
        /// <param name="apellido2"></param>
        /// <param name="dne"></param>
        /// <param name="tip"></param>
        /// <param name="empresa"></param>
        /// <param name="perfil"></param>
        /// <param name="telefono"></param>
        /// <returns></returns>
        public static bool ModificacionUsuario(string nombre, string apellido1, string apellido2, string dne, string tip, string empresa, string perfil, string telefono)
        {
            try
            {
                bool resultadoModificacion = UsersProvider.ModificarUsuario(nombre, apellido1, apellido2, dne, tip, empresa, perfil, telefono);
                return resultadoModificacion;
            }
            catch (Exception ex)
            {

                Debug.WriteLine("{0}{1}", ex.Message);
                return false;
            }
        }

        public static bool ActivarDesactivarUsuario(string idUsuario,string operacion)
        {
            try
            {
                bool resultadoModificacion = UsersProvider.ActivarDesactivarUsuario(idUsuario,operacion);
                return resultadoModificacion;
            }
            catch (Exception ex)
            {

                Debug.WriteLine("{0}{1}", ex.Message);
                return false;
            }
        }
    }
}
