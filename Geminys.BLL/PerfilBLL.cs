﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Geminys.DAL;
using Geminys.DAL.Users;
using System.ComponentModel.DataAnnotations;

namespace Geminys.BLL
{
    public class PerfilBLL
    {
        public int idPerfil { get; set; }
        [Required]
        public string nombrePerfil { get; set; }
        public bool activo { get; set; }

        /// <summary>
        /// Metodo que devuelve una lista de perfiles
        /// <return>Devuelve un objeto de tipo GEMINYS.DAL.Perfil</return>
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Perfil> GetAllPerfiles()
        {
            MenuProvider newprovider = new MenuProvider();
            var perfilesDAL = newprovider.GetPerfiles();           
            return perfilesDAL;
        }

        /// <summary>
        /// Metodo que inserta un nuevo nombre de perfil   
        /// </summary>
        public int AddPerfil(string nombrePerfil)
        {
            PerfilProvider newprovider = new PerfilProvider();
            return newprovider.AddPerfil(nombrePerfil); 

        }

        /// <summary>
        /// Metodo que envia una cadeda de idMenus 
        /// </summary>
        public int AddPerfilMenu(string strPerfilMenu, int idPerfil)
        {
            PerfilProvider newprovider = new PerfilProvider();
            return newprovider.AddPerfilMenu(strPerfilMenu, idPerfil);

        }
    }
}
