﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Geminys.DAL;

namespace Geminys.BLL.Users
{
   public class UsersManager
    {

       /// <summary>
       /// Metodo para obtener un listado con todos los usuarios
       /// </summary>
       /// <returns></returns>
        public Usuarios GetUsuario()
        {
            MenuProvider newprovider = new MenuProvider();
            var usuario = newprovider.GetUsuario();
            return usuario;
        }

    }
}
