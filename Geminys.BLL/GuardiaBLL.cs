﻿using Geminys.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Geminys.BLL
{
    public class GuardiaBLL
    {
        public int idDirectorioPersonal { get; set; }
        public string nombre { get; set; }
        public int idEmpresa { get; set; }
        public string servicio { get; set; }
        public string puesto { get; set; }
        public string email { get; set; }
        public string fijo { get; set; }
        public string movilCorporativo { get; set; }
        public string movilLargo { get; set; }
        public string comentarios { get; set; }
        public Nullable<bool> guardias { get; set; }
        public Nullable<bool> activo { get; set; }

        /// <summary>
        /// Constructor vacio
        /// </summary>
        public GuardiaBLL()
        {
        }

        /// <summary>
        /// Metodo que va a realizar la consulta que devuelve los datos con información referente al usuario logado
        /// <return>Devuelve un objeto de tipo GEMINYS.DAL.USUARIOS</return>
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Directorio_Personal> GetPersonalGuardia()
        {
            MenuProvider newprovider = new MenuProvider();
            var GuardiasDAL = newprovider.GetPersonalGuardia();

            return GuardiasDAL;
            
        }

    }
}
