﻿using Geminys.DAL;
using Geminys.DAL.Combos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Geminys.BLL
{
    /// <summary>
    /// Clase estatica para alimentar los distintos combos de la aplicacion
    /// </summary>
   public static class CombosBLL
    {
        /// <summary>
        /// Metodo estatico que devuelve un listado con las empresas activas asociadas a METRO
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<Empresas> getEmpresas () {

            IEnumerable<Empresas> listaEmpresas = null;
            listaEmpresas = ComboProvider.getListaEmpresas();
            return listaEmpresas ;
        }
        /// <summary>
        /// Metodo estatico que devuelve un listado con los perfiles activos
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<Perfil> getPerfiles() {

            IEnumerable<Perfil> listaPerfiles = null;
            listaPerfiles = ComboProvider.getListaPerfiles();
            return listaPerfiles;

        }
    }
}
